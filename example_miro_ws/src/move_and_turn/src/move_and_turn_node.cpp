#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <stdlib.h>


int main(int argc, char **argv)
{
	ros::init(argc, argv, "move_and_turn_node");
	ros::NodeHandle nh;
  // Connect to the appropriate topic, in this case, the simulation
	ros::Publisher pub = nh.advertise<geometry_msgs::Twist>("/miro/rob01/control/cmd_vel", 1000);

	geometry_msgs::Twist msg;

	ros::Rate rate(0.25);
	while(ros::ok()) {
		geometry_msgs::Twist msg;

		msg.linear.x = 3.0;

	  ROS_INFO_STREAM("MiRo will now move forward...");
		pub.publish(msg);

		rate.sleep();

	  msg.linear.x = 0.0;
	  msg.angular.z = 2.0;

	  ROS_INFO_STREAM("MiRo will now turn 180 degrees...");
		pub.publish(msg);

		rate.sleep();
	}

  return 0;
}
