#include <string>

#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <geometry_msgs/Twist.h>
#include <std_msgs/Float32MultiArray.h>
#include <std_msgs/UInt16MultiArray.h>


void wheelMovementTest(ros::Publisher& pub) {
	ros::Rate rate(0.5);
	geometry_msgs::Twist msg;

	msg.linear.x = 3.0;
	ROS_INFO_STREAM("MiRo will now move forward");
	pub.publish(msg);
	rate.sleep();

	msg.linear.x = -3.0;
	ROS_INFO_STREAM("MiRo will now move backwards");
	pub.publish(msg);
	rate.sleep();

	msg.linear.x = 0.0;
	msg.angular.z = 2.0;
	ROS_INFO_STREAM("MiRo will now turn 180 degrees");
	pub.publish(msg);
	rate.sleep();

	msg.angular.z = -2.0;
	ROS_INFO_STREAM("MiRo will now turn 180 degrees");
	pub.publish(msg);
	rate.sleep();
}


void cosmeticJointsTest(ros::Publisher& pub) {
	ros::Rate rate(0.5);
	std_msgs::Float32MultiArray msg;
	msg.data = {0.0, 0.0, 0.0, 0.0};

	ROS_INFO_STREAM("Testing eyelids");
	for (float i = 0.0; i < 1.25; i += 0.25) {
		msg.data[0] = i;
		pub.publish(msg);
		rate.sleep();
	}

	msg.data[0] = 0.0;
	msg.data[1] = 1.0;
	msg.data[2] = 1.0;
	msg.data[3] = 1.0;

	ROS_INFO_STREAM("Testing ears and tail");
	for (int i = 0; i < 5; i++) {
		pub.publish(msg);
		rate.sleep();
	}
}


void kinematicJointsTest(ros::Publisher& pub) {
	ros::Rate rate(0.25);
	sensor_msgs::JointState msg;
	// To establish the naming scheme for all of the float values
	// This is not needed, it is just semantic.
	msg.name = {"tilt", "lift", "yaw", "pitch"};
	msg.position = {0, 0, 0, 0}; // Start in default state
	msg.velocity = {1, 1, 1, 1}; // Do it slowly at 1 Rad/s

	ROS_INFO_STREAM("Moving joints to default state");
	pub.publish(msg);
	rate.sleep();

	ROS_INFO_STREAM("Moving joints to max positive state");
	msg.position = {1, 1, 1, 1};
	msg.velocity = {1, 1, 1, 1};
	pub.publish(msg);
	rate.sleep();

	ROS_INFO_STREAM("Moving joints to max negative state");
	msg.position = {-1, -1, -1, -1};
	msg.velocity = {1, 1, 1, 1};
	pub.publish(msg);
	rate.sleep();

	ROS_INFO_STREAM("Moving joints to default state (quickly)");
	msg.position = {0, 0, 0, 0};
	msg.velocity = {3, 3, 3, 3};
	pub.publish(msg);
	rate.sleep();
}


void lightsTest(ros::Publisher& pub) {
	ros::Rate rate(1);
	std_msgs::UInt16MultiArray msg;
	msg.data = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

	ROS_INFO_STREAM("Turning lights off");
	pub.publish(msg);

	for (char state = 1; state < 8; state++) {
		int r = 255 * (state & 4);
		int g = 255 * (state & 2);
		int b = 255 * (state & 1);

		rate.sleep();

		ROS_INFO_STREAM("For left side: R=" + std::to_string(r) + " G=" + std::to_string(g) + " B=" + std::to_string(b));
		for (int i = 0; i < 9; i += 3) {
			msg.data[i] = r;
			msg.data[i + 1] = g;
			msg.data[i + 2] = b;
		}
		pub.publish(msg);

		rate.sleep();

		ROS_INFO_STREAM("For right side: R=" + std::to_string(r) + " G=" + std::to_string(g) + " B=" + std::to_string(b));
		for (int i = 9; i < 18; i += 3) {
			msg.data[i] = r;
			msg.data[i + 1] = g;
			msg.data[i + 2] = b;
		}
		pub.publish(msg);
	}
}


int main(int argc, char* argv[]) {
	const std::string robNode = "rob01";

	ros::init(argc, argv, "ext_workout_node");

	if (argc < 2) {
		ROS_ERROR_STREAM("No robot specified, defaulting to rob01");
	} else {
		const std::string robNode(argv[1]);
	}

	ros::NodeHandle nh;
	ros::Publisher pubCosmetic = nh.advertise<std_msgs::Float32MultiArray>("/miro/" + robNode + "/control/cosmetic_joints", 10);
	ros::Publisher pubKinematic = nh.advertise<sensor_msgs::JointState>("/miro/" + robNode + "/control/kinematic_joints", 10);
	ros::Publisher pubLights = nh.advertise<std_msgs::UInt16MultiArray>("/miro/" + robNode + "/control/lights", 10);
	ros::Publisher pubWheels = nh.advertise<geometry_msgs::Twist>("/miro/" + robNode + "/control/cmd_vel", 10);

	ros::Rate rate(0.25);
	while(ros::ok()) {
		wheelMovementTest(pubWheels);
		cosmeticJointsTest(pubCosmetic);
		kinematicJointsTest(pubKinematic);
		lightsTest(pubLights);

		rate.sleep();
	}

  return 0;
}
