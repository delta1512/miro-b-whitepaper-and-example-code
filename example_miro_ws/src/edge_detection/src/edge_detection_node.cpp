#include <ros/ros.h>
#include <sensor_msgs/Range.h>
#include <std_msgs/UInt16MultiArray.h>
#include <geometry_msgs/Twist.h>
#include <stdlib.h>

int cliffL = 12;
int cliffR = 12;
float sonar = 0.0;


void cliffMsgRecv(const std_msgs::UInt16MultiArray cliffMsg) {
	cliffL = cliffMsg.data[0];
	cliffR = cliffMsg.data[1];

	ROS_INFO_STREAM("Cliff left " << cliffL << " Cliff right " << cliffR);
}


void sonarMsgRecv(const sensor_msgs::Range sonarMsg) {
	sonar = sonarMsg.range;

	ROS_INFO_STREAM("Sonar range is: " << sonar);
}


int main(int argc, char **argv)
{
	ros::init(argc, argv, "edge_detection_node");
	ros::NodeHandle nh;
  // Connect to the appropriate topic, in this case, the simulation
	ros::Publisher pub = nh.advertise<geometry_msgs::Twist>("/miro/rob01/control/cmd_vel", 1000);
	ros::Subscriber subCliff = nh.subscribe("/miro/rob01/sensors/cliff", 1000, &cliffMsgRecv);
	ros::Subscriber subSonar = nh.subscribe("/miro/rob01/sensors/sonar_range", 1000, &sonarMsgRecv);

	geometry_msgs::Twist msg;

	ros::Rate rate(0.5);
	while(ros::ok()) {
		ros::spinOnce();

		geometry_msgs::Twist msg;

		if (cliffL == 12 and cliffR == 12) {
			msg.linear.x = 0.5;
		} else {
			msg.linear.x = 0.0;
		}

		// sonar calculation here

	  ROS_INFO_STREAM("MiRo will now move linearly...");
		pub.publish(msg);

		rate.sleep();

	  msg.linear.x = 0.0;

		if (cliffL < 12) {
			msg.angular.z = 0.5;
		} else if (cliffR < 12) {
			msg.angular.z = 0.5;
		} else {
			continue;
		}

		// sonar calculation here

	  ROS_INFO_STREAM("MiRo will now turn 180 degrees...");
		pub.publish(msg);

		rate.sleep();
	}

  return 0;
}
